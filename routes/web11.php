<?php

use App\Http\Controllers\NutritionController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

Route::get('/', [NutritionController::class, 'index']);
Route::get('/create', [NutritionController::class, 'create'])->middleware('auth');    //pozivanje funkcije 'create'
Route::get('/update{id}', [NutritionController::class, 'update'])->middleware('auth');
Route::post('/movies', [NutritionController::class, 'store'])->middleware('auth');
Route::put('/movies/{id}', [NutritionController::class, 'edit'])->middleware('auth');
Route::delete('/movies/{id}', [NutritionController::class, 'destroy'])->middleware('auth');

Route::get('/registration', [UserController::class, 'registration']);
Route::get('/login', [UserController::class, 'login']);
Route::post('/user_registration', [UserController::class, 'registerUser']);
Route::post('/authentication', [UserController::class, 'authentication']);
Route::post('/logout', [UserController::class, 'logout'])->middleware('auth');


