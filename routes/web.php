<?php

use App\Http\Controllers\AboutMeController;
use App\Http\Controllers\NutritionController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;
//prvi
// Route::get('/options', function () {
//     return view('options');
// })->name('options');

Route::get('/', [NutritionController::class, 'index']);
Route::get('/blog', [NutritionController::class, 'index'])->name('blog');

Route::get('/create', [NutritionController::class, 'create'])->middleware('auth');    //pozivanje funkcije 'create'
Route::get('/update{id}', [NutritionController::class, 'update'])->middleware('auth');
Route::post('/nutritions', [NutritionController::class, 'store'])->middleware('auth');
Route::put('/nutritions/{id}', [NutritionController::class, 'edit'])->middleware('auth');
Route::delete('/nutritions/{id}', [NutritionController::class, 'destroy'])->middleware('auth');

Route::get('/registration', [UserController::class, 'registration']);
Route::get('/login', [UserController::class, 'login']);
Route::post('/user_registration', [UserController::class, 'registerUser']);
Route::post('/authentication', [UserController::class, 'authentication']);
Route::post('/logout', [UserController::class, 'logout'])->middleware('auth');

Route::get('/aboutme', [AboutMeController::class, 'show'])->name('aboutme.show');
Route::get('/aboutme/edit', [AboutMeController::class, 'edit'])->name('aboutme.edit');
Route::post('/aboutme/update', [AboutMeController::class, 'update'])->name('aboutme.update');

// Route::get('/aboutme', [AboutMeController::class, 'show']);

Route::get('/advices', [NutritionController::class, 'advice']);

