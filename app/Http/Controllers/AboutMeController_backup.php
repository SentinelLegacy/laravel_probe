<?php

namespace App\Http\Controllers;

use App\Models\AboutMe;
use Illuminate\Http\Request;

class AboutMeController extends Controller
{
    public function index()
    {
        return view('aboutme.aboutme',
    [
        'aboutme' => AboutMe::all()
    ]);
    }

    public function update($id)
    {
        $aboutme = AboutMe::find($id);      //podatci o filmu postaju dostupni unutar view-a update.blade.php
        return view('aboutme.update_about',
        [
            'aboutme' => $aboutme
        ]);
    }
}
