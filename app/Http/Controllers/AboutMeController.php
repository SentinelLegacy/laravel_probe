<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class AboutMeController extends Controller
{
    public function show()
    {
        // Pretpostavljamo da je korisnik s ID-om 1 autor bloga
        $user = User::find(1);

        return view('aboutme.show', ['user' => $user]);
    }

    public function edit()
    {
        // Logika za dohvaćanje podataka o tebi, npr. iz baze podataka ili iz nekog drugog izvora
        $aboutme = 'Ovdje možete staviti svoj opis ili informacije o sebi';

        return view('aboutme.edit', ['aboutme' => $aboutme]);
    }

    public function update(Request $request)
    {
        // Pronalaženje trenutno prijavljenog korisnika
        $user = auth()->user();

        // Provjera postojanja korisnika i je li on autor
        if ($user && $user->id === 1) {
            // Ažuriranje informacija o autoru
            User::where('id', $user->id)->update(['about_me' => $request->aboutme]);

            // Redirekcija s porukom o uspjehu
            return redirect()->route('aboutme.edit')->with('success', 'Informacije su uspješno ažurirane.');
        } else {
            // Redirekcija s porukom o grešci
            return redirect()->route('aboutme.edit')->with('error', 'Nemate ovlasti za uređivanje informacija.');
        }
    }
}
