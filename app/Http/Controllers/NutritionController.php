<?php

namespace App\Http\Controllers;

use App\Models\Nutrition;
use Illuminate\Http\Request;

class NutritionController extends Controller
{
    public function index()
    {
        return view('nutritions.nutritions',        //kada se blade nalazi unutar odredjenog direktorija
    [
        'nutritions' => Nutrition::all()
    ]);
    }

    public function advice()
    {
        return view('nutritions.advices',        //kada se blade nalazi unutar odredjenog direktorija
    [
        'advices' => Nutrition::all()
    ]);
    }

    //INDEX SAMO SA IZBORNIKOM
//     public function index()
// {
//     return view('options'); // Vraća samo izbornik
// }

    public function create()
    {   
    // Kreirajte novi prazni prehrambeni proizvod
    $nutrition = new Nutrition();

    // Proslijedite novi prehrambeni proizvod u pogled
    return view('nutritions.create', compact('nutrition'));
    }

    // public function create()
    // {
    //     return view('nutritions.create');
    // }   

    public function store(Request $request)
    {
        $formFields = $request->validate([
            'name' => 'required',
            'weight' => 'required',
            'description' => 'required'
        ]);

        Nutrition::create($formFields);

        return redirect('/');
    }

    public function update($id)
    {
        $nutrition = Nutrition::find($id);      //podatci o filmu postaju dostupni unutar view-a update.blade.php
        return view('nutritions.update',
        [
            'nutrition' => $nutrition
        ]);
    }

    public function edit(Request $request, $id)
    
    {
        $nutrition = Nutrition::find($id);

        $nutrition['name'] = $request->name;
        $nutrition['weight'] = $request->weight;
        $nutrition['description'] = $request->description;

        $nutrition->save();
        return redirect('/');
        //dd($request->all());
    }

    public function destroy($id)
    {
        $nutrition = Nutrition::find($id);
        $nutrition->delete();
        return redirect('/');
    }
}
