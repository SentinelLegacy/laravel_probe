<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    public function registration()
    {
        return view ('users.registration');
    }
    
    public function registerUser(Request $request)
    {
        $formFields = $request->validate([
            'name' => 'required',
            'email' => ['required', 'email', Rule::unique('users', 'email')],
            'password' => 'required|confirmed'
            //dd($request->all());
        ]);

        $formFields['password'] = bcrypt($formFields['password']);

        $user = User::create($formFields);

        //Login
        auth()->login($user);

        return redirect('/');
    }

    public function logout(Request $request)
    {
        auth()->logout();

        $request->session()->invalidate();  //omogućuje da se sa trenutnim tokenom više ništa ne može učiniti, odnosno da više nije validan
        $request->session()->regenerateToken();

        return redirect('/');

    }
    public function login()
    {
        return view('users.login');
    }

    public function authentication(Request $request)
    {
        //dd($request->all());
        $formFields = $request->validate([
            'email' => ['required', 'email'],
            'password' => 'required'
            //dd($request->all());
        ]);

        if (auth()->attempt($formFields))  //attempt vraća je li se korisnik uspješno prijavio
        {
            $request->session()->regenerate();
            return redirect('/');
        }

        return back();
    }
}
