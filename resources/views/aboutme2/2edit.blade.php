<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Uredi o sebi</title>
    <!-- Dodajte CSS ovdje -->
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h2>Uredi o sebi</h2>
                <form method="POST" action="{{ route('aboutme.update') }}">
                    @csrf
                    <div class="mb-3">
                        <label for="aboutme" class="form-label">O meni</label>
                        <textarea name="aboutme" class="form-control" id="aboutme" rows="5">{{ $aboutme }}</textarea>
                    </div>
                    <button type="submit" class="btn btn-danger">Spremi</button>
                </form>
            </div>
        </div>
    </div>
    <!-- Dodajte JavaScript ovdje -->
</body>
</html>


