<x-layout>
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <h2>Uredi opis o meni</h2>
            <form method="POST" action="/aboutme/update">
                @csrf
                <div class="mb-3">
                    <label for="aboutme" class="form-label">Opis</label>
                    <textarea name="aboutme" class="form-control" id="aboutme" rows="5">{{$aboutme}}</textarea>
                </div>
                <button type="submit" class="btn btn-primary">Spremi</button>
            </form>
        </div>
    </div>
</x-layout>
