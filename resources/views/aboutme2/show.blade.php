<x-layout>
    <div class="container">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <h2>O meni</h2>
                <p>Ime mi je Ivan, Od organskih proteinskih praškova do superhrane, pažljivo odabirem proizvode koji podržavaju životni stil i preporučujem ih zainteresiranima. Moj cilj nije samo prodaja proizvoda, već educiranje ljudi o važnosti pravilne prehrane za zdrav i sretan život.

                    Aktivno sudjelujem u zajednici putem radionica, online seminara i blogova, pružajući vrijedne informacije i podršku onima koji žele unaprijediti svoje prehrambene navike. Moja strast prema nutricionizmu očituje se u svakom aspektu vlastitog rada, od nježnih savjeta za početnike do dubokih analiza znanstvenih istraživanja.</p>
                @auth
                    @if (auth()->user()->id === 1) <!-- Provjerava da li je trenutno prijavljeni korisnik sa ID-em 1 -->
                        <a href="{{ route('aboutme.edit') }}" class="btn btn-primary" style="background-color: #fd7e14;">Uredi</a> <!-- Link za uređivanje, vodi na edit rutu -->
                    @endif
                @endauth
            </div>
        </div>
    </div>
</x-layout>