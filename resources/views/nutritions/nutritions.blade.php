<x-layout>
    @auth
    <div class="row">
        <div class="col-sm-2">
            <a class="btn btn-primary" href="/create" style="background-color: #92aa59;">Dodavanje novog proizvoda</a>   <!-- kad je ovaj redak   -->

           <!-- redak za ikonu dodavanja proizvoda -->
    </div>
</div>
@endauth
    <div class="row">
        <div class="col-sm-12">

        </div>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Naziv proizvoda</th>
                <th scope="col">Pakiranje</th>
                @auth
                    <th scope="col">O proizvodu</th>
                @endauth
            </tr>
            </thead>
            <tbody>
                @foreach ($nutritions as $key=>$nutrition)  <!-- prikazivanje podataka -->
                <tr>
                    <th scope="row">{{($key+1)}}</th>
                    <td>{{$nutrition->name}}</td>
                    <td>{{$nutrition->weight}}</td>
                    <td>{{$nutrition->description}}</td>
                    @auth
                    <td>
                        <a class="btn btn-secondary" href="/update{{$nutrition->id}}" style="background-color: #20c997;">Izmijeni podatke</a>
                        <form method="POST" action="/nutritions/{{$nutrition->id}}">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger" type="submit" style="background-color: #198754;">Izbriši proizvod</a>
                        </form>
                    </td>
                    @endauth
                </tr>
                @endforeach
            
    </div>
</x-layout>