<x-layout>
    <div class="row">
        <div class="mb-3">
            <h2>Izmjena mog opisa</h2>
        </div>
        <form method="POST" action="/nutritions/{{$nutrition->id}}">
            @csrf
            @method("PUT")
            <div class="mb-3">
              <label for="exampleInputName" class="form-label">O meni</label>
              <input type="text" name="name" value="{{$nutrition->name}}" class="form-control" id="exampleInputName" aria-describedby="nameName">
            </div>
            <button type="submit" class="btn btn-primary">Spremi promjene</button>
        </form>
    </div>
</x-layout>