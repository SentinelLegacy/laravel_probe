<x-layout>
    <div class="row">
        <div class="mb-3">
            <h2>Izmijeni unešene podatke o proizvodu</h2>
        </div>
        <form method="POST" action="/nutritions/{{$nutrition->id}}">
            @csrf
            @method("PUT")
            <div class="mb-3">
              <label for="exampleInputName" class="form-label">Naziv proizvoda</label>
              <input type="text" name="name" value="{{$nutrition->name}}" class="form-control" id="exampleInputName" aria-describedby="nameName">
            </div>
            <div class="mb-3">
                <label for="exampleInputWeight" class="form-label">Pakiranje</label>
                <input type="text" name="weight" value="{{$nutrition->weight}}" class="form-control" id="exampleInputWeight" aria-describedby="nameWeight">
            </div>
            <div class="mb-3">
                <label for="exampleInputDescription" class="form-label">O proizvodu</label>
                <input type="text" name="description" value="{{$nutrition->description}}" class="form-control" id="exampleInputDescription" aria-describedby="nameDescription">
            </div>
            <button type="submit" class="btn btn-primary" style="background-color: #fd7e14;">Snimi promjene</button>
        </form>
    </div>
</x-layout>