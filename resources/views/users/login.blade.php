<x-layout>
    <div class="row">
        <div class="mb-3">
            <h2>Prijava korisnika</h2>
        </div>
        <form method="POST" action="/authentication">
            @csrf
            <div class="mb-3">
                <label for="exampleInputEmail" class="form-label">Email</label>
                <input type="email" name="email" class="form-control" id="exampleInputEmail" aria-describedby="nameEmail">
            </div>
            <div class="mb-3">
                <label for="exampleInputPassword" class="form-label">Lozinka</label>
                <input type="password" name="password" class="form-control" id="exampleInputPassword" aria-describedby="namePassword">
            </div>
            <button type="submit" class="btn btn-primary" style="background-color: #dc3545;">Prijava</button>
          </form>
    </div>
</x-layout>