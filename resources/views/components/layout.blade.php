<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Nutrition</title>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #e3fdf4;">
    <div class="container-fluid">
        <a class="nav-link disabled" href="/" tabindex="-1" aria-disabled="true">IZBORNIK</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                @auth
                <li class="nav-item">
                    <form method="POST" action="/logout">
                        @csrf
                        <button class="btn btn-light" type="submit">Odjavi se</button>
                    </form>
                </li>
                @else
                <li class="nav-item">
                    <a class="nav-link" href="/registration">Registriraj se</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/login">Prijava</a>
                </li>
                @endauth
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="/blog">Blog</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/aboutme">About me</a>
                </li>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/advices">Savjeti o prehrani</a>
                </li>
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Racun
                  </a>
                  <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <li><a class="dropdown-item" href="/registration">Registriraj se</a></li>
                    <li><a class="dropdown-item" href="/login">Prijava</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="container">
    {{$slot}}
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

</body>
</html>
