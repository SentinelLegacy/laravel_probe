<x-layout>
    <div class="row">
        <div class="col-sm-12">
        </div>
        <table class="table">
            <thead>
            <tr>
                @auth
                    <th scope="col">O meni</th>
                @endauth
            </tr>
            </thead>
            <tbody>
                @foreach ($aboutme as $key=>$about_me)  <!-- prikazivanje podataka -->
                <tr>
                    <th scope="row">{{($key+1)}}</th>
                    <td>{{$about_me->aboutme}}</td>
                    @auth
                    <td>
                        <a class="btn btn-secondary" href="/update{{$about_me->id}}">Uredi opis</a>
                    </td>
                    @endauth
                </tr>
                @endforeach
    </div>
</x-layout>